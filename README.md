# To run the application

Install dependencies using below command.

### `npm install`

Once dependencies are installed run the project using below command

### `npm run dev`

Runs the app in the development mode.\

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

Open json-server [http://localhost:3001](http://localhost:3001) to view it in your browser.

Please find the recording for demo - https://drive.google.com/file/d/1mYEgAoSEEOUBWiq7EVRfbaVBg-3ej_To/view?usp=share_link
