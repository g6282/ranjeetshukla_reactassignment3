import React, { useState, useEffect } from "react";
import Home from "./pages/home/home";
import axios from "axios";
import Moviedetail from "./pages/movie_details/moviedetail";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function Main() {
  const [selectedHeading, setSelectedHeading] = useState("movies-in-theaters");
  const [movieList, setMovieList] = useState([]);
  const [tempMovieList, setTempMovieList] = useState([]);
  const [isOpenDetailPage, setIsOpenDetailPage] = useState(false);
  const [detailPageData, setDetailPageData] = useState([]);

  const onClickMovieHeading = (menu) => {
    setSelectedHeading(menu);
    fetchMovieData(menu);
  };

  const handleSearch = (e) => {
    const filteredMovieList = tempMovieList.filter((movie) => {
      const movieName = movie.title.toLowerCase();
      return movieName.includes(e.target.value.toLowerCase());
    });
    console.log("filtered", e.target.value);
    setMovieList(filteredMovieList);
  };

  const onCardClick = (movieDetail) => {
    setDetailPageData(movieDetail);
    setIsOpenDetailPage(true);
  };
  const onFavoriteClick = (movieDetail, wantAddInFavorite) => {
    axios.defaults.baseURL = "http://localhost:3001/";
    if (wantAddInFavorite) {
      axios({
        method: "post",
        url: "favourit",
        data: movieDetail,
      })
        .then((data) => {
          if (data.status == "201") {
            toast.success("Successfully added in favorite");
          }
        })
        .catch(() => {
          toast.error("Alredy added in favorite");
        });
    } else {
      axios({
        method: "delete",
        url: "favourit/" + movieDetail.id,
      }).then((data) => {
        if (data.status == "200") {
          toast.success("Successfully removed favorite");
          fetchMovieData(selectedHeading);
        }
      });
    }
  };

  const handleBack = () => {
    setIsOpenDetailPage(false);
    setDetailPageData([]);
  };

  useEffect(() => {
    fetchMovieData(selectedHeading);
  }, []);

  const fetchMovieData = (slug) => {
    axios.defaults.baseURL = "http://localhost:3001/";
    axios.get(slug).then(function (response) {
      setMovieList(response.data);
      setTempMovieList(response.data);
    });
  };
  return (
    <div>
      {!isOpenDetailPage && (
        <Home
          onClickMovieHeading={onClickMovieHeading}
          selectedHeading={selectedHeading}
          onSearch={handleSearch}
          movieList={movieList}
          onCardClick={onCardClick}
          onFavoriteClick={onFavoriteClick}
        />
      )}
      {isOpenDetailPage && (
        <Moviedetail movieDetailRow={detailPageData} handleBack={handleBack} />
      )}
      <ToastContainer />
    </div>
  );
}

export default Main;
