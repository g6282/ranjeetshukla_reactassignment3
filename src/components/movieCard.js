import React from "react";

function MovieCard({
  movieDetails,
  onCardClick,
  onFavoriteClick,
  selectedHeading,
}) {
  return (
    <div className="movie-card-container">
      <div
        className="movie-poster-container"
        onClick={() => onCardClick(movieDetails)}
      >
        <img className="movie-poster" src={movieDetails.posterurl} />
      </div>
      <div className="movie-title">{movieDetails.title}</div>
      <div
        className="movie-favorite"
        onClick={() =>
          onFavoriteClick(movieDetails, !(selectedHeading == "favourit"))
        }
      >
        {selectedHeading == "favourit"
          ? "Remove from favorites"
          : "Add to favorites"}
        <span className="heart-icon">
          {selectedHeading == "favourit" ? (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
              <path d="M175 175C184.4 165.7 199.6 165.7 208.1 175L255.1 222.1L303 175C312.4 165.7 327.6 165.7 336.1 175C346.3 184.4 346.3 199.6 336.1 208.1L289.9 255.1L336.1 303C346.3 312.4 346.3 327.6 336.1 336.1C327.6 346.3 312.4 346.3 303 336.1L255.1 289.9L208.1 336.1C199.6 346.3 184.4 346.3 175 336.1C165.7 327.6 165.7 312.4 175 303L222.1 255.1L175 208.1C165.7 199.6 165.7 184.4 175 175V175zM512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256zM256 48C141.1 48 48 141.1 48 256C48 370.9 141.1 464 256 464C370.9 464 464 370.9 464 256C464 141.1 370.9 48 256 48z" />
            </svg>
          ) : (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
              <path d="M47.6 300.4L228.3 469.1c7.5 7 17.4 10.9 27.7 10.9s20.2-3.9 27.7-10.9L464.4 300.4c30.4-28.3 47.6-68 47.6-109.5v-5.8c0-69.9-50.5-129.5-119.4-141C347 36.5 300.6 51.4 268 84L256 96 244 84c-32.6-32.6-79-47.5-124.6-39.9C50.5 55.6 0 115.2 0 185.1v5.8c0 41.5 17.2 81.2 47.6 109.5z" />
            </svg>
          )}
        </span>
      </div>
    </div>
  );
}

export default MovieCard;
