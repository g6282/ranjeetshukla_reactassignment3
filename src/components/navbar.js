import React from "react";

const createMenu = (onClickMovieHeading, menuName, menu_slug, selectedMenu) => {
  return (
    <span
      onClick={() => onClickMovieHeading(menu_slug)}
      className={selectedMenu === menu_slug ? "active" : ""}
      key={menu_slug}
    >
      {menuName}
    </span>
  );
};

function Navbar({ onClickMovieHeading, selectedHeading }) {
  const menusArr = [
    { menu_name: "Movies in theaters", menu_slug: "movies-in-theaters" },
    { menu_name: "Coming soon", menu_slug: "movies-coming" },
    { menu_name: "Top rated indian", menu_slug: "top-rated-india" },
    { menu_name: "Top rated movies", menu_slug: "top-rated-movies" },
    { menu_name: "Favourits", menu_slug: "favourit" },
  ];

  return (
    <>
      {menusArr.map((menuObj) =>
        createMenu(
          onClickMovieHeading,
          menuObj.menu_name,
          menuObj.menu_slug,
          selectedHeading
        )
      )}
    </>
  );
}

export default Navbar;
