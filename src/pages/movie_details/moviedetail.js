import React from "react";
import "./movieDetail.css";

function Moviedetail({ movieDetailRow, handleBack }) {
  console.log("movieDetailRow--", movieDetailRow);
  return (
    <div>
      <div className="back-to-home">
        <span onClick={handleBack}>Back to home</span>
      </div>
      <div className="detail-container">
        <div className="poster-container">
          <img src={movieDetailRow.posterurl} />
        </div>
        <div className="movie-full-detail">
          <div className="movie-heading">{`${movieDetailRow.title} (${movieDetailRow.year})`}</div>
          <div className="description">
            <table>
              <tr>
                <td>Imdb Rating</td>
                <td>{movieDetailRow.imdbRating}</td>
              </tr>
              <tr>
                <td>Content Rating</td>
                <td>{movieDetailRow.contentRating}</td>
              </tr>
              <tr>
                <td>Average Rating</td>
                <td>{movieDetailRow.averageRating}</td>
              </tr>
              <tr>
                <td>Duration</td>
                <td>{movieDetailRow.duration}</td>
              </tr>
              <tr>
                <td>Genres</td>
                <td>{movieDetailRow.genres.join(", ")}</td>
              </tr>
              <tr>
                <td>Actors</td>
                <td>{movieDetailRow.actors.join(", ")}</td>
              </tr>
              <tr>
                <td>Release Date</td>
                <td>{movieDetailRow.releaseDate}</td>
              </tr>
              <tr>
                <td>Story line</td>
                <td>{movieDetailRow.storyline}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Moviedetail;
