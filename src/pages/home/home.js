import React from "react";
import "./home.css";
import Navbar from "../../components/navbar";
import MovieCard from "../../components/movieCard";

function Home({
  onClickMovieHeading,
  selectedHeading,
  onSearch,
  movieList,
  onCardClick,
  handleBack,
  onFavoriteClick
}) {
  return (
      <div className="home-wrapper">
        <div className="menu-wrapper">
          <div className="menus">
            <Navbar
              onClickMovieHeading={onClickMovieHeading}
              selectedHeading={selectedHeading}
            />
          </div>
          <div className="search-wrapper">
            <input
              onKeyUp={onSearch}
              type="text"
              placeholder="Search movies by name ..."
            />
          </div>
        </div>
        <div className="main-title"> Movie</div>
        {movieList && (
          <div className="main-home-container">
            {movieList.map((movieRow) => (
              <MovieCard
                movieDetails={movieRow}
                onCardClick={onCardClick}
                handleBack={handleBack}
                onFavoriteClick={onFavoriteClick}
                selectedHeading={selectedHeading}
              />
            ))}
            {movieList.length <= 0 && <div className="no-data-found ">No data found</div>}
          </div>
        )}
      </div>
  );
}

export default Home;
